Ext.define('MobileOutreach.field.Telephone', {
    extend: 'Ext.field.Text',
    xtype: 'telephonefield',

    config: {
        /**
         * @cfg
         * @inheritdoc
         */
        component: {
            type: 'tel'
        },

        /**
         * @cfg
         * @inheritdoc
         */
        ui: 'number'
    },

    applyPlaceHolder: function(value) {
        // Android 4.1 & lower require a hack for placeholder text in number fields when using the Stock Browser
        // details here https://code.google.com/p/android/issues/detail?id=24626
        this._enableNumericPlaceHolderHack = ((!Ext.feature.has.NumericInputPlaceHolder) && (!Ext.isEmpty(value)));
        return value;
    },

    onFocus: function(e) {
        if (this._enableNumericPlaceHolderHack) {
            this.getComponent().input.dom.setAttribute("type", "tel");
        }
        this.callParent(arguments);
    },

    onBlur: function(e) {
        if (this._enableNumericPlaceHolderHack) {
            this.getComponent().input.dom.setAttribute("type", "text");
        }
        this.callParent(arguments);
    },

    doInitValue : function() {
        var value = this.getInitialConfig().value;

        if (value) {
            value = this.applyValue(value);
        }

        this.originalValue = value;
    },

    applyValue: function(value) {
        return value;
    },

    getValue: function() {
        var value = parseFloat(this.callParent(), 10);
        return (isNaN(value)) ? null : value;
    },

    doClearIconTap: function(me, e) {
        me.getComponent().setValue('');
        me.getValue();
        me.hideClearIcon();
    }
});