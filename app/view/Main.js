Ext.define('MobileOutreach.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar'
    ],

    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Welcome',
                iconCls: 'home',

                xtype: 'welcomeView',

                items: {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'IHS Mobile Outreach'
                }
            },
            {
                title: 'Resources',
                iconCls: 'organize',

                xtype: 'resourcesView',

                items: {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Resources'
                }
            },
            {
                title: 'Report',
                iconCls: 'compose',
                xtype: 'reportView'
            },
            {
                title: 'Contact IHS',
                iconCls: 'info',

                xtype: 'contactIHSView',

                items: {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Contact IHS'
                }
            },
            {
                title: 'Settings',
                iconCls: 'settings',

                xtype: 'settingsView',

                items: {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Settings'
                }
            }
        ]
    }
});
