Ext.define('MobileOutreach.view.Report', {
    extend: 'Ext.form.Panel',
    requires: ['MobileOutreach.field.Telephone'],
    xtype: 'reportView',

    config: {
        layout: 'vbox',
        styleHtmlContent: true,

        items: [{
            docked: 'top',
            xtype: 'titlebar',
            title: 'Report Homeless'
        },{
            xtype: 'label',
            html: 'If you see a homeless camp, please take a photo and submit so that our teams can visit.'
        },{
            xtype: 'button',
            text: 'Take Photo',
            margin: '1em 0 1em 0',
            iconCls: 'add'
        },{
            xtype: 'fieldset',
            instructions: 'Please write a detailed description to describe the area and what you have seen.',
            items: [{
                xtype: 'textareafield',
                label: 'Notes',
                name: 'notes',
                maxRows: 3
            }]
        },{
            xtype: 'fieldset',
            instructions: 'Your contact information is not required, but preferred.',
            items: [{
                xtype: 'emailfield',
                label: 'Email',
                autoCorrect: false,
                autoCapitalize: false
            },{
                xtype: 'telephonefield',
                label: 'Phone',
                autoCorrect: false
            }]
        },{
            xtype: 'button',
            text: 'Submit Report',
            margin: '0 0 1em 0'
        }]
    },



});
