Ext.define('MobileOutreach.view.Resources', {
    extend: 'Ext.Container',
    xtype: 'resourcesView',

    config: {
        html: 'resources'
    }
});
